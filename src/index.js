import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';
import reducer from './store/reducer';
import { Provider as ReduxProvider } from 'react-redux';

const store = createStore(reducer);

ReactDOM.render(<ReduxProvider store={ store }><App /></ReduxProvider>, document.getElementById('root'));
registerServiceWorker();
