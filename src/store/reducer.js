const initialState = {
  user: {
    isLoggedIn: false,
    data: {
      preferences: []
    },
  },
  errors: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER':
      let newRegisteredUsers = [];

      if (localStorage.getItem('users')) {
        let oldRegisteredUsers = JSON.parse(localStorage.getItem('users'));
        newRegisteredUsers = [...oldRegisteredUsers];
        newRegisteredUsers.push(action.payload);
      } else {
        newRegisteredUsers.push(action.payload);
      }

      localStorage.setItem('users', JSON.stringify(newRegisteredUsers));
      localStorage.setItem('currentUser', JSON.stringify(action.payload));

      return {
        ...state,
        user: {
          isLoggedIn: true,
          ...state.user.data,
          data: {
            name: action.payload.name,
            email: action.payload.email,
            username: action.payload.username
          }
        }
      };
    case 'LOGIN':
      if (localStorage.getItem('users')) {
        let oldRegisteredUsers = JSON.parse(localStorage.getItem('users'));
        const { username, password } = action.payload;
        let user = oldRegisteredUsers.filter(el => {
          return el.username === username && el.password === password
        });
        if (user.length > 0) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          return {
            ...state,
            user: {
              isLoggedIn: true,
              ...state.user.data,
              data: {
                name: user.name,
                email: user.email,
                username: user.username
              }
            }
          };
        }
      }
    case 'LOGOUT':
      localStorage.removeItem('currentUser');
      return {
        ...state,
        user: {
          isLoggedIn: false,
          data: {
            preferences: []
          }
        }
      };
    default:
      if (localStorage.getItem('currentUser'))
        return {
          ...state,
          user: {
            isLoggedIn: true,
            ...state.user.data,
            data: JSON.parse(localStorage.getItem('currentUser'))
          }
        };
      return state;
  }
}

export default reducer;
