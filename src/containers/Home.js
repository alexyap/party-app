import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Header from '../components/Header';

class Home extends Component {
  render() {
    console.log(this.props)
    const { user } = this.props;

    if (!user.isLoggedIn) {
      return <Redirect to="/login" />
    }

    return (
      <div>
        <Header />
        Home
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(Home);
