import React, { Component } from 'react';
import { connect } from 'react-redux';
import { is_valid } from '../lib/helpers';
import { Redirect, Link } from 'react-router-dom';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

class Register extends Component {
  state = {
    formData: {
      name: '',
      email: '',
      username: '',
      password: '',
      passwordConfirmation: ''
    }
  }

  handleChange = event => {
    const { target } = event;
    let formData = {...this.state.formData};
    formData[target.name] = target.value;
    this.setState({ formData });
  }

  handleSubmit = event => {
    event.preventDefault();
    const { formData } = this.state;
    if (is_valid(formData)) {
      this.props.register(formData);
    }
  }

  render() {
    const { user } = this.props;
    const { formData } = this.state;
    const divStyle = {
      margin: 'calc((100vh - 434px) / 2) auto'
    };

    if (user.isLoggedIn) {
      return <Redirect to="/" />
    }

    return (
      <Grid style={divStyle}>
        <Row>
          <Col md={8} mdOffset={2}>
            <p>Have an account? <Link to="/login">Login</Link></p>
            <form onSubmit={ event => this.handleSubmit(event) }>
              <FormGroup controlId="name">
                <ControlLabel>Name</ControlLabel>
                <FormControl type="name"
                             value={ formData.name }
                             name="name"
                             onChange={ this.handleChange }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <FormGroup controlId="email">
                <ControlLabel>Email</ControlLabel>
                <FormControl type="email"
                             value={ formData.email }
                             name="email"
                             onChange={ this.handleChange }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <FormGroup controlId="username">
                <ControlLabel>Username</ControlLabel>
                <FormControl type="username"
                             value={ formData.username }
                             name="username"
                             onChange={ this.handleChange  }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <FormGroup controlId="password">
                <ControlLabel>Password</ControlLabel>
                <FormControl type="password"
                             value={ formData.password }
                             name="password"
                             onChange={ this.handleChange  }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <FormGroup controlId="passwordConfirmation">
                <ControlLabel>Username</ControlLabel>
                <FormControl type="password"
                             value={ formData.passwordConfirmation }
                             name="passwordConfirmation"
                             onChange={ this.handleChange }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <Button bsStyle="primary" type="submit">Sign Up</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return {
    register: (formData) => dispatch({ type: 'REGISTER', payload: formData })
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Register);
