import React, { Component } from 'react';
import { connect } from 'react-redux';
import { is_valid } from '../lib/helpers';
import { Redirect, Link } from 'react-router-dom';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

class Login extends Component {
  state = {
    formData: {
      username: '',
      password: ''
    }
  }

  handleChange = event => {
    const { target } = event;
    let formData = {...this.state.formData};
    formData[target.name] = target.value;
    this.setState({ formData });
  }

  handleSubmit = event => {
    event.preventDefault();
    const { formData } = this.state;
    if (is_valid(formData)) {
      this.props.login(formData);
    }
  }

  render() {
    const { user } = this.props;
    const { formData } = this.state;
    const divStyle = {
      margin: 'calc((100vh - 212px) / 2) auto'
    };

    if (user.isLoggedIn) {
      return <Redirect to="/" />
    }

    return (
      <Grid style={divStyle}>
        <Row>
          <Col md={8} mdOffset={2}>
            <p>Don't have an account? <Link to="/register">Sign up</Link></p>
            <form onSubmit={ event => this.handleSubmit(event) }>
              <FormGroup controlId="username">
                <ControlLabel>Username</ControlLabel>
                <FormControl type="text"
                             value={ formData.username }
                             name="username"
                             onChange={ this.handleChange }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <FormGroup controlId="password">
                <ControlLabel>Password</ControlLabel>
                <FormControl type="password"
                             value={ formData.password }
                             name="password"
                             onChange={ this.handleChange }
                />
              {/*<FormConrol.Feedback />*/}
              </FormGroup>
              <Button bsStyle="primary" type="submit">Login</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return {
    login: (formData) => dispatch({ type: 'LOGIN', payload: formData })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
