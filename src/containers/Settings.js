import React, { Component } from 'react';
import { connect } from 'react-redux';

class Settings extends Component {
  state = {

  }

  componentDidMount() {

  }

  render() {
    return (
      <div>
        Settings
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps)(Settings);
