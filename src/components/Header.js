import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Header = props => {
  const logout = event => {
    event.preventDefault();
    props.logout();
  }

  return (
    <nav>
      <a onClick={ event => logout(event) }>Logout</a>
      <Link to="/settings">Settings</Link>
    </nav>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch({ type: 'LOGOUT' })
  }
}

export default connect(null, mapDispatchToProps)(Header);
