/*
* Validates form data
*
* @param object
*
* @return boolean
*/
const is_valid = (formData) => {
  let errors = {};
  for (let key in formData) {
    if (!formData[key])
      errors[key] = `${key} is required.`;
  }

  if (Object.keys(errors).length === 0)
    return true;

  return false;
}

const capitalize = () => {

}

export {
  is_valid,
  capitalize
}
